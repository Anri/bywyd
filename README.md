# Bywyd

## Usage

Placer le fichier [`bywyd.sty`](./bywyd.sty) au même niveau que votre `.tex`.

Dans le fichier `.tex` :

```latex
\usepackage{bywyd}
```

Options disponibles :

| Option | Utilité                                |
| :----: | :------------------------------------- |
|  `fr`  | Utilisation de `babel` et plan traduit |
| `blue` | Thème bleu                             |

Commandes disponibles :

|           Commande           | Utilité                               |
| :--------------------------: | :------------------------------------ |
| `\cvAuthorName{prénom}{nom}` | Défini le prénom/nom de l'auteur      |
|  `\cvDocumentTitle{titre}`   | Défini le titre du document           |
|        `\cvFirstname`        | Affiche le prénom de l'auteur         |
|        `\cvLastname`         | Affiche le nom de famille de l'auteur |
|          `\cvTitle`          | Affiche le titre du document          |
|            `\tb`             | Affiche un point médian               |
|       `\cvSkillTwoNR`        | Équivalent à `\cvSkillTwo` sans notes |
|         `\ul{texte}`         | Souligne du texte                     |

## [Exemples](./examples)

Voir les [exemples](./examples) d'utilisation et voir la
[page CTAN de limecv](https://ctan.org/pkg/limecv) pour des exemples plus
concrets.
